
test_label <- "train_xgb_model"
testthat::test_that(
  test_label,
  {

    # Inputs

    df <- tibble::tibble(
      v1 = 1:100,
      v2 = rep(2, 100),
      v3 = rep(0, 100)
    )
    train_df <- df[1:80, ]
    test_df <- df[81:100, ]

    target_col <- "v2"
    index_col <- "v1"
    weight <- rep(1, 80)


    # Core
    res <- train_xgb_model(
      train_df,
      test_df,
      target_col,
      index_col,
      weight,
      nrounds = 5
    )
    xgb_model <- res$model

    expect_s3_class(
      xgb_model,
      class = "xgb.Booster"
    )
  }
)
