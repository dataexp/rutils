
test_label <- "as_posixct_or_null"
testthat::test_that(
  test_label,
  {

    # Inputs

    tz <- "US/Central"
    other_tz <- "Europe/Paris"

    datetime_null <- NULL
    datetime_string <-"2022-02-24 13:45:45"
    datetime_posixct_right_tz <- datetime_string %>%
      as.POSIXct(tz = tz)
    datetime_posixct_other_tz <- datetime_string %>%
      as.POSIXct(tz = other_tz)
    datetime_date <- "2022-02-24" %>%
      as.Date()
    datetime_num <- 45

    # Core

    res_datetime_null <- datetime_null %>%
      as_posixct_or_null(tz = tz)
    res_datetime_string <- datetime_string %>%
      as_posixct_or_null(tz = tz)
    res_datetime_posixct_right_tz <- datetime_posixct_right_tz %>%
      as_posixct_or_null(tz = tz)
    res_datetime_posixct_other_tz <- datetime_posixct_other_tz %>%
      as_posixct_or_null(tz = tz)

    # Excectations

    testthat::expect_null(
      res_datetime_null
    )
    testthat::expect_equal(
      res_datetime_string,
      datetime_posixct_right_tz
    )
    testthat::expect_equal(
      res_datetime_posixct_right_tz,
      datetime_posixct_right_tz
    )
    testthat::expect_equal(
      res_datetime_posixct_other_tz,
      datetime_posixct_right_tz %>%
        lubridate::`%m-%`(
          lubridate::hours(7)
        )
    )
    testthat::expect_error(
      datetime_num %>%
        as_posixct_or_null(tz = tz)
    )
  }
)
