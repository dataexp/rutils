# Functions mainly used in loaders to simplify dbplyr queries, and tibble handling

#' Filter by valid conditions (NULL right side of expression are discarded)
#' Usefull if a condition based on argument war be either defined, either NULL
#' for example "start_at"
#'
#' @param df A tibble like dbplyr
#' @param ... The filter to apply if valid
#'
#' @return Filtered df
#' @export
filter_by_valid_conditions <- function(tb, ...) {
  quosures <- enquos(...)
  # Do keep quosure if right side of filter is not NULL
  do_keep_quosure <- quosures %>%
    lapply(
      \(x) {
        x %>%
          rlang::quo_get_expr() %>%
          as.character() %>%
          `[`(3) %>%
          identical("NULL") %>%
          isFALSE()
      }
    ) %>%
    unlist()
  sub_quosures <- quosures[do_keep_quosure]
  if (inherits(tb, "tbl_sql")) {
    filtered_tb <- tb %>%
      dplyr::filter(sub_quosures)
  } else {
    filtered_tb <- tb %>%
      dplyr::filter(!!!sub_quosures)
  }
  return(filtered_tb)
}


#' Apply a call or anotherto a tibble depending on the value of a condition
#'
#' @param df A tibble
#' @param condition A condition
#' @param call_if_true Call to apply if true
#' @param call_if_false Call to apply if false
#'
#' @return df after call
#' @export
call_ifelse <- function(df, condition, call_if_true, call_if_false = NULL) {
  if (rlang::eval_tidy(enquo(condition), df)) {
    res <- quo(df %>% !!enquo(call_if_true)) %>%
      rlang::quo_squash() %>%
      rlang::eval_tidy()
  } else {
    if (rlang::quo_is_null(enquo(call_if_false))) {
      res <- df
    } else {
      res <- quo(df %>% !!enquo(call_if_false)) %>%
        rlang::quo_squash() %>%
        rlang::eval_tidy()
    }
  }
  return(res)
}
