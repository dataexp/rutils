# Functions used to manage logging


#' On staging or production environments, copy tryCatchLog dump file to S3
#'
#' @param bucket Name of the S3 bucket
#' @export
copy_error_dump_to_bucket <- function(bucket) {
  if (Sys.getenv("ENV") == "local") {
    return()
  }
  if (!getOption("tryCatchLog.write.error.dump.file")) {
    return()
  }
  dump_folder <- getOption("tryCatchLog.write.error.dump.folder")
  dump_files <- file.info(
    dir(
      path = dump_folder,
      full.names = TRUE,
      pattern = "*.rda"
    )
  ) %>%
    tibble::rownames_to_column() %>%
    dplyr::mutate(
      filename = rowname %>% basename()
    ) %>%
    dplyr::pull(filename)
  if (length(dump_files) == 0) {
    return()
  }
  for (dump_file in dump_files) {
    aws.s3::put_object(
      file = file.path(dump_folder, dump_file),
      object = file.path("error_dumps", dump_file),
      bucket = bucket
    )
  }
  return()
}
