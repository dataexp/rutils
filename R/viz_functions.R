# Functions used to generate graphs


#' Save graph to html file
#'
#' @param graph The graph to save
#' @param path The path where to write the file
#' @param libdir The directory where to write the graphical libs
#' @export
save_graph_to_html <- function(graph, path, libdir = "lib") {
  if (tools::file_ext(path) != "html") {
    stop('Use ".html" as extension.')
  }
  abs_root_dir <- file.path(getwd(), dirname(path))
  dir.create(abs_root_dir, showWarnings = FALSE)
  abs_file_path <- file.path(abs_root_dir, basename(path))
  abs_lib_path <- file.path(abs_root_dir, libdir)
  htmltools::save_html(
    graph,
    abs_file_path,
    libdir = abs_lib_path
  )
}


#' Save graphs identified by variable names to path as html
#'
#' @param graphnames The list of graph variables to save
#' @param path The path to save files to
#' @export
save_graph_list_to_html <- function(graphnames, path) {
  for (graphname in graphnames) {
    graph <- get(graphname)
    graph %>%
      save_graph_to_html(file.path(path, sprintf("%s.html", graphname)))
  }
  return()
}



#' Generates a ggplotly graph for a single serie in a time tibble
#'
#' @export
#' @import ggplot2
viz_df <- function(df,
                   xcol = "date",
                   ycol = NULL,
                   title = "",
                   xlab = "",
                   ylab = "",
                   colors = NULL,
                   size = 0.2,
                   height = NULL,
                   return_plotly = T) {
  df_long <- df %>%
    tidyr::pivot_longer(
      -c(xcol),
      names_to = "var",
      values_to = "value"
    )
  p <- ggplot(df_long) +
    geom_line(
      aes_string(x = xcol, y = "value", color = "var"),
      size = size
    ) +
    theme_light() +
    theme(
      panel.border = element_blank(),
      panel.background = element_blank(),
      legend.position = "none"
    ) +
    xlab(xlab) +
    ylab(ylab) +
    theme(axis.title.y=element_text(angle=0)) +
    expand_limits(y = 0)
  if (!is.null(colors)) {
    p <- p +
      scale_color_manual(values = colors)
  }
  if (return_plotly) {
    p <- p %>%
      plotly::ggplotly(height = height) %>%
      plotly::layout(
        annotations = list(
          list(
            x = 0,
            y = 1.05,
            text = title,
            showarrow = F,
            xref = "paper",
            yref = "paper"
          )
        ),
        xaxis = list(
          rangeslider = list(type = xcol)
        ),
        yaxis = list(title = ylab),
        xaxis = list(title = xlab)
      )
  }

  return(p)
}


#' @export
pick_colors <- function(n, palette = "Dark2") {
  colors <- grDevices::colorRampPalette(
    RColorBrewer::brewer.pal(8, palette)
  )(n)
  return(colors)
}


#' Generates stacked ggplotly graphs for series in a time tibble
#'
#' @export
viz_dff <- function(
    df,
    xcol = "date",
    height = NULL,
    size = 0.5,
    return_plotly = T
) {
  ycols <- names(df)[names(df) != xcol]
  colors <- pick_colors(length(ycols))
  names(colors) <- ycols
  p_list <- ycols %>%
    lapply(
      \(ycol) {
        sub_df <- df[, c(xcol, ycol)]
        sub_df %>%
          viz_df(
            title = ycol,
            color = colors[ycol],
            height = height,
            size = size,
            return_plotly = return_plotly
          )
      }
    )
  if (return_plotly) {
    n_rows <- length(p_list)
    p <- plotly::subplot(
      p_list,
      nrows = n_rows,
      shareX = TRUE,
      margin = c(0.07, 0.0, 0.02/n_rows, 0.02/n_rows)
    )
  } else {
    p <- gridExtra::grid.arrange(p_list, ncol = 1)
  }
  return(p)
}


#' @export
viz_ts <- function(ts, title = NULL, title_from_col = F, group = "ensync", useDataTimezone = T) {
  if (!xts::is.xts(ts)) {
    ts <- ts %>%
      df_to_xts()
  }
  if (title_from_col) {
    title <- paste(colnames(ts), sep = "_")
  }
  dyg <- ts %>%
    dygraphs::dygraph(main = title, group = group, width = "100%") %>%
    dygraphs::dySeries() %>%
    dygraphs::dyRangeSelector() %>%
    dygraphs::dyOptions(useDataTimezone = useDataTimezone)
  return(dyg)
}

#' @export
viz_tss <- function(ts, title_from_col = F, group = "ensync", useDataTimezone = T) {
  if (!xts::is.xts(ts)) {
    ts <- ts %>%
      df_to_xts()
  }
  dyg_lst <- colnames(ts) %>%
    lapply(
      function(col) {
        ts[, col] %>%
          viz_ts(
            title_from_col = title_from_col,
            group = group,
            useDataTimezone = useDataTimezone
          )
      }
    )
  return(dyg_lst)
}

#' @export
add_units_to_dyg_lst <- function(dyg_lst, units_vec) {
  if (length(dyg_lst) != length(units_vec)) {
    stop("dyg_lst and units_vec must have the same length.")
  }
  for (i in 1:length(dyg_lst)) {
    dyg_lst[[i]] <- dyg_lst[[i]] %>%
      dygraphs::dyAxis("y", label = units_vec[[i]])
  }
  return(dyg_lst)
}
