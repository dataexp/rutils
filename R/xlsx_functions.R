#' Reads all sheets from a Excel file and returns a list of dataframes
#'
#' @export
read_xlsx_to_df_list <- function(filename, tibble = FALSE) {
  sheets <- readxl::excel_sheets(filename)
  df_list <- lapply(sheets, \(x) readxl::read_excel(filename, sheet = x))
  if (!tibble) df_list <- df_list %>% lapply(as.data.frame)
  names(df_list) <- sheets
  return(df_list)
}


#' Writes a list of dataframes to sheets of an Excel file
#'
#' @export
write_df_list_to_xlsx <- function(df_list, filename) {
  wb <- openxlsx::createWorkbook()
  startRow <- 1
  if (length(df_list) > 0) {
    for (i in 1:length(df_list)) {
      df <- df_list[[i]]
      sheetName <- names(df_list)[[i]]
      openxlsx::addWorksheet(
        wb = wb,
        sheetName = sheetName,
        gridLines = FALSE
      )
      if (nrow(df) == 0) next()
      col_types <- openxlsx::writeDataTable(
        wb = wb,
        sheet = i,
        x = df,
        startRow = startRow
      )
      openxlsx::setColWidths(
        wb = wb,
        sheet = i,
        1:ncol(df),
        widths = "auto"
      )
    }
  }
  openxlsx::saveWorkbook(wb, filename, overwrite = TRUE)
}

