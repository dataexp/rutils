# Functions used to check data quality

#' Calculates the ratio of rows containing NA in a time tibble
#'
#' @param df A tibble
#' @param date_col The column of df containing the time index
#' @return The ratio of NA rows in df
#' @export
calculate_na_ratio <- function(df, date_col = "date") {
  ref_cols <- c(date_col)
  value_cols <- colnames(df)[!colnames(df) %in% ref_cols]
  na_df <- df %>%
    dplyr::filter_at(
      dplyr::vars(value_cols),
      dplyr::any_vars(is.na(.))
    )
  na_ratio <- (nrow(na_df) / nrow(df)) %>%
    round(3)
  return(na_ratio)
}
