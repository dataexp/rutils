% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils.R
\name{df_to_tibble}
\alias{df_to_tibble}
\title{Transforms an object to a tibble if it is a dataframe}
\usage{
df_to_tibble(x)
}
\description{
Transforms an object to a tibble if it is a dataframe
}
