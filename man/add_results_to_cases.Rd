% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/tests_management_functions.R
\name{add_results_to_cases}
\alias{add_results_to_cases}
\title{Adds results to cases}
\usage{
add_results_to_cases(cases, result_names, dirs_list)
}
\arguments{
\item{cases}{A tibble of tests cases}

\item{result_names}{The results names to add}

\item{dirs_list}{A list of directories where results are stored
for each test_name}
}
\value{
the extended tibble of test cases
}
\description{
Adds results to cases
}
